#include <stdio.h>
#include <math.h>

// F(x,y,z)
double F(double x, double y, double z){
    return exp(x)*sin(x+y)/(pow(z,2))+sqrt(z*x/y);
}
// ΠΕΠΕΡΑΣΜΕΝΕΣ ΔΙΑΦΟΡΕΣ
void FD(double x,double y,double z,double *dF_dx,double *dF_dy,double *dF_dz,double *val) {
  double epsilon = 0.0001;
	*val=F(x,y,z);
	*dF_dx=(F(x+epsilon,y,z)-F(x-epsilon,y,z))/(2*epsilon);
	*dF_dy=(F(x,y+epsilon,z)-F(x,y-epsilon,z))/(2*epsilon);
	*dF_dz=(F(x,y,z+epsilon)-F(x,y,z-epsilon))/(2*epsilon);      
}
// ΠΡΟΚΕΚΥΨΕ ΑΠΟ ΤΟ TAPENADE
double F_d(double x, double xd, double y, double yd, double z, double zd,double *F) {
    double arg1;
    double arg1d;
    double result1;
    double result1d;
    double temp;
    double temp0;
    double temp1;
    temp = z*x/y;
    arg1d = (x*zd+z*xd-temp*yd)/y;
    arg1 = temp;
    temp = sqrt(arg1);
    result1d = (arg1 == 0.0 ? 0.0 : arg1d/(2.0*temp));
    result1 = temp;
    temp = pow(z, 2);
    temp0 = exp(x)/temp;
    temp1 = sin(x + y);
    *F = temp1*temp0 + result1;
    return temp0*cos(x+y)*(xd+yd) + temp1*(exp(x)*xd-temp0*2*pow(z, 2-1)*zd)/
    temp + result1d;
}
int main(void){
	double x=0.01,y=1,z=1,val;
	double dF_dx,dF_dy,dF_dz;
    printf("******\n* FD *\n******\n");
    FD(x,y,z,&dF_dx,&dF_dy,&dF_dz,&val);
	printf("dF/dx= %f \tdF/dy= %f \tdF/dz= %f\n",dF_dx,dF_dy,dF_dz);
	printf("******\n* AD *\n******\n");
    val = F(x,y,z);
	dF_dx=F_d(x,1,y,0,z,0,&val);
	dF_dy=F_d(x,0,y,1,z,0,&val);
	dF_dz=F_d(x,0,y,0,z,1,&val);
	printf("dF/dx= %f \tdF/dy= %f \tdF/dz= %f\n",dF_dx,dF_dy,dF_dz);
} 
