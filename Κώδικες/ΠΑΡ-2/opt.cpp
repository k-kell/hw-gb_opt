#include "primal.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void datain(vector<double> &b) {
  fstream input;
  input.open("input.dat", ios::in);
  for (int i = 0; i < b.size(); i++) {
    input >> b[i];
  }
  input.close();
}
void FD(int &NX, vector<double> &b, vector<double> &SX, vector<double> &SY,
        vector<double> &T, vector<double> &dF_db) {
  double epsilon = 0.001;
  vector<double> bplus(8),bminus(8);
  double Fplus, Fminus;
  //double F = primal(NX, b, SX, SY, T);
  for (int i = 0; i < b.size(); i++) {
    bplus = b;
    bplus[i] = b[i] + epsilon;
    bminus = b;
    bminus[i] = b[i] - epsilon;
    Fplus = primal(NX, bplus, SX, SY, T);
    Fminus = primal(NX, bminus, SX, SY, T);
    dF_db[i] = (Fplus - Fminus) / (2*epsilon);
    //dF_db[i] = (Fplus - F) / (epsilon);
    cout << dF_db[i] <<"\t";
  }
  cout<<endl;
}
int main() {
  int NX = 400;
  vector<double> SX(NX+1, 0), SY(NX+1, 0);
  vector<double> T(NX / 2 +1);
  vector<double> b(8), dF_db(8),abserrs(8),resid;
  datain(b);
  double hta = 0.1;
  double err = 1;
  int i=1;
  while (err>=1e-6){
    cout << "ΕΠΑΝΑΛΗΨΗ : "<< i << endl;
    FD(NX, b, SX, SY, T, dF_db);
 
    for (int k = 0; k < b.size(); k++) {
      b[k] -= hta * dF_db[k];
      abserrs[k]=abs(dF_db[k]);
    }
    err=*max_element( abserrs.begin(), abserrs.end());
    resid.push_back(err);
    i+=1;
  }
  fstream Final;
  Final.open("final.dat",ios::out); 
  for (int k = 0; k < b.size(); k++) {
    Final << b[k] << endl;
  }
  Final.close();
  fstream residuals;
  residuals.open("res.dat",ios::out);
  for (int k = 0; k < resid.size(); k++) {
    residuals << k+1 <<"\t"<< resid[k] << endl;
  }
  residuals.close();

  cout << primal(NX, b, SX, SY, T)<<endl;
}
