#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
using namespace std;
int factorial(int n);
double nCn(int a, int i);
void coeffs(vector<vector<double>> &mCoeffs);
void MatMul(vector<vector<double>> &mCoeffs, vector<double> &t_vec,
            vector<double> &C_t, vector<double> &t_vec_dot,
            vector<double> &C_t_dot);
void BezierCurve(vector<double> &XCP, vector<double> &YCP, vector<double> &SX,
                 vector<double> &SY, vector<vector<double>> &mCoeffs, double dx,
                 int NX);
double primal_eq(double a, double g, int M_1, int M_2, double S, double x,
                 double T);
double trapez(vector<double> &T, double &dx, int &NX);
double primal(int &NX, vector<double> &b, vector<double> &SX,
              vector<double> &SY, vector<double> &T);
