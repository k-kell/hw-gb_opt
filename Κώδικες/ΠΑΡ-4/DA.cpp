#include "primal.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void datain(vector<double> &b) {
  fstream input;
  input.open("input.dat", ios::in);
  for (int i = 0; i < b.size(); i++) {
    input >> b[i];
  }
  input.close();
}

void MatMul(int &N, vector<vector<double>> &dR_db, vector<double> &PSI,
            vector<double> &PSIR) {
  PSIR.assign(8, 0);
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < N; j++) {
      PSIR[i] += PSI[j] * dR_db[j][i];
    }
  }
}

void tridiag(vector<double> &a, vector<double> &b, vector<double> &c,
             vector<double> &d, vector<double> &res, int size) {
  vector<double> cdot(size, 0.0), ddot(size, 0.0);
  cdot[0] = c[0] / b[0];
  ddot[0] = d[0] / b[0];
  for (int i = 1; i < size - 1; i++) {
    cdot[i] = c[i] / (b[i] - a[i] * cdot[i - 1]);
  }
  for (int i = 1; i < size; i++) {
    ddot[i] = (d[i] - a[i] * ddot[i - 1]) / (b[i] - a[i] * cdot[i - 1]);
  }
  res.back() = ddot.back();
  for (int i = size - 1; i > -1; i--) {
    res[i] = ddot[i] - cdot[i] * res[i + 1];
  }
}

void DA(int &NX, vector<double> &b, vector<double> &SX, vector<double> &SY,
        vector<double> &T, vector<double> &dF_db,
        vector<vector<double>> &dS_db) {
  vector<double> thetaF_thetab(8, 0), dF_dT(NX / 2+1, 0), PSI(NX /2 +1, 0),
      PSIR(8, 0);
  vector<vector<double>> dR_db(NX / 2 + 1, vector<double>(8, 0));
  vector<double> dR_dT_minus(NX / 2 + 1, 0), dR_dT(NX / 2 + 1, 0),
      dR_dT_plus(NX / 2 + 1, 0);
  float M_1 = 1, M_2 = 1;
  double dx = 2 * b[0] / NX;
  double mid = T[NX / 4];
  cout<<primal(NX, b, SX, SY, T, dS_db)<<endl;
  // dF/dT
  dF_dT[0] = (T[0] - mid) * dx;
  dF_dT.back() = (T.back() - mid) * dx;
  for (int i = 1; i < NX / 2; i++) {
    dF_dT[i] = 2 * (T[i] - mid) * dx;
  }

  // θF/θb
  double temp = 0;
  for (int l = 0; l < NX / 2; l++) {
    temp += pow(T[l] - mid, 2) + pow(T[l + 1] - mid, 2);
  }
  thetaF_thetab[0] = temp / NX;

  // dR_db
  dR_db[0][0] = 1 / pow(b[0], 2) - 50 / pow(b[0], 3);
  for (int i = 1; i < NX / 2; i++) {
    dR_db[i][0] = -(T[i + 1] - T[i-1]) * NX /(4*pow(b[0], 2))+ 4 * (2 + b[2] * M_2) *2* i * T[i] / NX;
    dR_db[i][1] = -M_1 * pow(T[i], 2) * SY[2 * i];
    dR_db[i][2] = 4 * M_2 * i * dx * T[i];
    for (int j = 3; j < 8; j++) {
      dR_db[i][j] = -(1 + b[1] * M_1) * pow(T[i], 2) * dS_db[2 * i][j];
    }
  }
    dR_db[NX/2][0] = -(T.back() - T[NX/2-1]) * NX /(2*pow(b[0], 2))+ 4 * (2 + b[2] * M_2) *b[0]* T.back() ;
    dR_db[NX/2][1] = -M_1 * pow(T.back(), 2) * SY.back();
    dR_db[NX/2][2] = 4 * M_2 * b[0]*T.back();
    dR_db[NX/2][3] = -(1 + b[1] * M_1) * pow(T.back(), 2) * dS_db[NX][3];
    dR_db[NX/2][4] = -(1 + b[1] * M_1) * pow(T.back(), 2) * dS_db[NX][4];
    dR_db[NX/2][5] = -(1 + b[1] * M_1) * pow(T.back(), 2) * dS_db[NX][5];
    dR_db[NX/2][6] = -(1 + b[1] * M_1) * pow(T.back(), 2) * dS_db[NX][6];
    dR_db[NX/2][7] = -(1 + b[1] * M_1) * pow(T.back(), 2) * dS_db[NX][7];
  // dR_dT
  dR_dT[0] = 1;
  for (int i = 1; i < NX / 2; i++) {
    dR_dT_minus[i] = -1 / (2 * dx);
    dR_dT[i] = -2 * (1 + b[1] * M_1) * T[i] * SY[2 * i] +4 * (2 + b[2] * M_2) * i * dx;
    dR_dT_plus[i] = +1 / (2 * dx);
  }
  dR_dT_minus.back() = -1 / (2 * dx);
  dR_dT.back() = 1 / (2 * dx) - 2 * (1 + b[1] * M_1) * T.back() * SY.back() +
                 4 * (2 + b[2] * M_2) * b[0];
  int N = NX / 2 + 1;
  PSI.assign(NX / 2 + 1, 0);
  tridiag(dR_dT_plus, dR_dT, dR_dT_minus, dF_dT, PSI, N);
  MatMul(N, dR_db, PSI, PSIR);
  for (int i = 0; i < 8; i++) {
    thetaF_thetab[i] -= PSIR[i];
  }
  for (int i = 0; i < 8; i++) {
    dF_db[i] = thetaF_thetab[i];
    cout << dF_db[i] << "\t";
  }
  cout << endl;
  // Ψ=gauss(dR_dU',dF_dU')
  // dF_db=thetaF_thetab - Ψ'*dR_db
}

int main() {
  int NX = 400;
  vector<double> SX(NX + 1, 0), SY(NX + 1, 0);
  vector<double> T(NX / 2 + 1);
  vector<double> b(8), dF_db(8),abserrs(8),resid;
  vector<vector<double>> dS_db(NX + 1, vector<double>(8, 0));
  datain(b);
  double hta = 1.5;
  double err = 1;
  int i=1;
  while (err>=1e-6){
    cout << "ΕΠΑΝΑΛΗΨΗ : "<< i << endl;
    DA(NX, b, SX, SY, T, dF_db,dS_db);
 
    for (int k = 0; k < b.size(); k++) {
      b[k] -=hta * dF_db[k];
      abserrs[k]=abs(dF_db[k]);
    }
    err=*max_element( abserrs.begin(), abserrs.end());
    resid.push_back(err);
    i+=1;
  }
  fstream Final;
  Final.open("final.dat",ios::out); 
  for (int k = 0; k < b.size(); k++) {
    Final << b[k] << endl;
  }
  Final.close();
  fstream residuals;
  residuals.open("res.dat",ios::out);
  for (int k = 0; k < resid.size(); k++) {
    residuals << k+1 <<"\t"<< resid[k] << endl;
  }
  residuals.close();

  cout <<   primal(NX, b, SX, SY, T, dS_db)<<endl;
}
