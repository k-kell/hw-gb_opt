#include "primal.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void datain(vector<double> &b) {
  fstream input;
  input.open("input.dat", ios::in);
  for (int i = 0; i < b.size(); i++) {
    input >> b[i];
  }
  input.close();
}

double primal_deriv(vector<double> &b, float &M_1, float &M_2, double S, double dS,
                    double x, double T, double dam, double dgm, double Y) {
  return 2 * T * S * (1 + b[1] * M_1) * Y - (4 * (2 + b[2] * M_2)) * x * Y +
         dam * pow(T, 2) * S - dgm * x * T + dS * (1 + b[1] * M_1) * pow(T, 2);
}
void DD(int &NX, vector<double> &b, vector<double> &SX, vector<double> &SY,
        vector<double> &T, vector<double> &dF_db,
        vector<vector<double>> &dS_db) {
  vector<double> dT_dB(NX / 2+1);
  double dam, dgm, ini_cond;
  float M_1 = 1, M_2 = 1;
  double dx = 4 * b[0] / NX;
  cout<<primal(NX, b, SX, SY, T, dS_db)<<endl;
  for (int i = 0; i < 8; i++) {

    // CALCULATE COEFFS OF EQUATION
    if (i == 1) {
      dam = M_1;
    } else {
      dam = 0;
    }
    if (i == 2) {
      dgm = 4 * M_2;
    } else {
      dgm = 0;
    }
    if (i == 0) {
      ini_cond = -1 / pow(b[0], 2) + 50 / pow(b[0], 3);
    } else {
      ini_cond = 0;
    }
    // RUNGE KUTTA
    double x, k1, k2, k3, k4;
    dT_dB[0] = ini_cond;
    for (int k = 0; k < NX / 4; k++) {
      x = k * dx;
      k1 = dx * primal_deriv(b, M_1, M_2, SY[4 * k], dS_db[4 * k][i], x,
                             T[2 * k], dam, dgm, dT_dB[k]);
      k2 = dx * primal_deriv(b, M_1, M_2, SY[4 * k + 2], dS_db[4 * k + 2][i],
                             x + dx / 2, T[2 * k + 1], dam, dgm,
                             dT_dB[k] + k1 / 2);
      k3 = dx * primal_deriv(b, M_1, M_2, SY[4 * k + 2], dS_db[4 * k + 2][i],
                             x + dx / 2, T[2 * k + 1], dam, dgm,
                             dT_dB[k] + k2 / 2);
      k4 =
          dx * primal_deriv(b, M_1, M_2, SY[4 * (k + 1)], dS_db[4 * (k + 1)][i],
                            x + dx, T[2 * (k + 1)], dam, dgm, dT_dB[k] + k3);
      dT_dB[k + 1] = dT_dB[k] + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
    }

    // TRAPEZ
    double temp = 0;
    double mid = T[NX / 8], mid_d = dT_dB[NX / 8];
    for (int l = 0; l < NX / 4; l++) {
      temp += 2 * (dT_dB[l] - mid_d) * (T[l] - mid) +
              2 * (dT_dB[l + 1] - mid_d) * (T[l + 1] - mid);
    }
    temp = dx * temp / 2;
    dF_db[i] = temp;
    if (i==0){
        dF_db[i]+=T.back()-mid;
    }
    temp = 0;
    cout << dF_db[i] << "\t";
  }
  cout << endl;
}
int main() {
  int NX = 400;
  vector<double> SX(NX+1, 0), SY(NX+1, 0);
  vector<double> T(NX / 2+1);
  vector<double> b(8), dF_db(8),abserrs(8),resid;
  vector<vector<double>> dS_db(NX+1, vector<double>(8, 0));
  datain(b);
  double hta = 0.1;
  double err = 1;
  int i=1;
  while (err>=1e-6){
    cout << "ΕΠΑΝΑΛΗΨΗ : "<< i << endl;
    DD(NX, b, SX, SY, T, dF_db,dS_db);
 
    for (int k = 0; k < b.size(); k++) {
      b[k] -= hta * dF_db[k];
      abserrs[k]=abs(dF_db[k]);
    }
    err=*max_element( abserrs.begin(), abserrs.end());
    resid.push_back(err);
    i+=1;
  }
  fstream Final;
  Final.open("final.dat",ios::out); 
  for (int k = 0; k < b.size(); k++) {
    Final << b[k] << endl;
  }
  Final.close();
  fstream residuals;
  residuals.open("res.dat",ios::out);
  for (int k = 0; k < resid.size(); k++) {
    residuals << k+1 <<"\t"<< resid[k]<<endl;
  }
  residuals.close();

  cout <<   primal(NX, b, SX, SY, T, dS_db)<<endl;
}
