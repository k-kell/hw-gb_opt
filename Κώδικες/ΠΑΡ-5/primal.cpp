#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>

using namespace std;

int factorial(int n) {
  int res = 1, i;
  for (i = 2; i <= n; i++)
    res *= i;
  return res;
}

double nCn(int a, int i) {
  if (a - i < 0) {
    return 0;
  }
  return factorial(a) / (factorial(i) * factorial(a - i));
}

void coeffs(vector<vector<double>> &mCoeffs) {
  for (int i = 0; i < 7; i++) {
    for (int j = 0; j < 7; j++) {
      mCoeffs[i][j] = pow(-1, j - i) * nCn(6, j) * nCn(j, i);
    }
  }
}
void MatMul(vector<vector<double>> &mCoeffs, vector<double> &t_vec,
            vector<double> &C_t, vector<double> &t_vec_dot,
            vector<double> &C_t_dot) {
  C_t.assign(7, 0);
  C_t_dot.assign(7, 0);
  for (int i = 0; i < 7; i++) {
    for (int j = 0; j < 7; j++) {
      C_t[i] += mCoeffs[i][j] * t_vec[j];
      C_t_dot[i] += mCoeffs[i][j] * t_vec_dot[j];
    }
  }
}

void BezierCurve(vector<double> &XCP, vector<double> &YCP, vector<double> &SX,
                 vector<double> &SY, vector<vector<double>> &mCoeffs, double dx,
                 int NX, vector<vector<double>> &dS_db) {
  vector<double> C_t(7, 0), C_t_dot(7, 0), t_vec(7), t_vec_dot(7);
  double ST, ST_dot;

  for (int i = 0; i < NX + 1; i++) {
    SX[i] = i * dx;
    double err = 1, t_new;
    double t_old = 0.5;

    // NEWTON-RAPHSON METHOD
    while (err >= 0.01) {
      for (int j = 0; j < 7; j++) {
        t_vec[j] = pow(t_old, j);
        t_vec_dot[j] = j * pow(t_old, j - 1);
      }
      ST = 0;
      ST_dot = 0;
      MatMul(mCoeffs, t_vec, C_t, t_vec_dot, C_t_dot);
      for (int k = 0; k < 7; k++) {
        ST += XCP[k] * C_t[k];
        ST_dot += XCP[k] * C_t_dot[k];
      }
      ST -= SX[i];
      if (ST_dot == 0) {
        t_new = t_old;
      } else {
        t_new = t_old - ST / ST_dot;
      }
      err = abs(t_new - t_old);
      t_old = t_new;
    }
    for (int j = 0; j < 7; j++) {
      t_vec[j] = pow(t_old, j);
      t_vec_dot[j] = pow(j * t_old, j - 1);
    }

    MatMul(mCoeffs, t_vec, C_t, t_vec_dot, C_t_dot);
    // FINAL S(x) not S(t)
    for (int k = 0; k < 7; k++) {
      SY[i] += YCP[k] * C_t[k];
    }
    // CALC dS/dBn for optimization
    for (int k = 1; k < 6; k++) {
      dS_db[i][k + 2] = C_t[k];
    }
  }
}
// PRIMAL EQUATION FOR R-K METHOD
double primal_eq(double a, double g, int M_1, int M_2, double S, double x,
                 double T) {
  return (1 + a * M_1) * pow(T, 2) * S - 4 * x * T * (2 + g * M_2);
}
// CALCULATE COST FUNCTION
double trapez(vector<double> &T, double &dx, int &NX) {
  double Tmid = T[NX / 4];
  double F = 0;
  for (int i = 0; i < NX / 2 + 1; i++) {
    F += pow(T[i] - Tmid, 2) + pow(T[i + 1] - Tmid, 2);
  }
  F = dx * F / 2;
  return F;
}
// MAIN FUNCTION 
double primal(int &NX, vector<double> &b, vector<double> &SX,
              vector<double> &SY, vector<double> &T,
              vector<vector<double>> &dS_db) {
  float M_1 = 1, M_2 = 1;
  double L, a, g;
  L = b[0];
  a = b[1];
  g = b[2];
  vector<double> XCP(7), YCP(7);
  for (int i = 1; i < 6; i++) {
    YCP[i] = b[i + 2];
  }
  vector<vector<double>> mCoeffs(7, vector<double>(7));
  // discretezation of control points
  XCP[0] = 0;
  XCP[6] = L;
  XCP[1] = XCP[0] + L / 4;
  XCP[5] = XCP[6] - L / 4;
  XCP[2] = XCP[1] + L / 6;
  XCP[4] = XCP[5] - L / 6;
  XCP[3] = XCP[2] + L / 12;
  YCP[0] = 1 + M_2 / 10; // t-0 => S(0)=YCP[O]
  YCP[6] = 3 + M_1 / 10; // t-1 => S(L)=YCP[6]

  // int NX = 400;
  double dx = L / NX;

  // coeffs in matrix form
  coeffs(mCoeffs);

  SX.assign(NX + 1, 0);
  SY.assign(NX + 1, 0);

  // creates points given C.P's and dt
  BezierCurve(XCP, YCP, SX, SY, mCoeffs, dx, NX, dS_db);
  // writes S(x)
  fstream output;
  output.open("bezier.dat", ios::out);
  for (int i = 0; i < NX + 1; i++) {

    output << SX[i] << "\t" << SY[i] << endl;
  }
  output.close();

  // writes C.P's
  fstream bezcp;
  bezcp.open("cp.dat", ios::out);
  for (int i = 0; i < 7; i++) {

    bezcp << XCP[i] << "\t" << YCP[i] << endl;
  }
  bezcp.close();


  double x, k1, k2, k3, k4;
  dx = 2 * dx;
  T[0] = 5 + 1 / L - pow(5 / L, 2);
  // RUNGE-KUTTA 4TH ORDER
  for (int i = 0; i < NX / 2; i++) {
    x = i * dx;
    k1 = dx * primal_eq(a, g, M_1, M_2, SY[2 * i], x, T[i]);
    k2 = dx *
         primal_eq(a, g, M_1, M_2, SY[2 * i + 1], x + dx / 2, T[i] + k1 / 2);
    k3 = dx *
         primal_eq(a, g, M_1, M_2, SY[2 * i + 1], x + dx / 2, T[i] + k2 / 2);
    k4 = dx * primal_eq(a, g, M_1, M_2, SY[2 * (i + 1)], x + dx, T[i] + k3);
    T[i + 1] = T[i] + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
  }
  // writes T(x)
  fstream outpuT;
  outpuT.open("T.dat", ios::out);
  for (int i = 0; i < NX / 2 + 1; i++) {

    outpuT << i * dx << "\t" << T[i] << endl;
  }
  outpuT.close();
  return trapez(T, dx, NX);
}
