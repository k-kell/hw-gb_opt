#include "primal.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

void datain(vector<double> &b) {
  fstream input;
  input.open("input.dat", ios::in);
  for (int i = 0; i < b.size(); i++) {
    input >> b[i];
  }
  input.close();
}

double psi_deriv(vector<double> &b, int &M_1, int &M_2, double &S, double x,
                 double &T, double &mid, double Y) {
  return -2 * T * S * (1 + b[1] * M_1) * Y + (4 * (2 + b[2] * M_2)) * x * Y +
         2 * (T - mid);
}
void CA(int &NX, vector<double> &b, vector<double> &SX, vector<double> &SY,
        vector<double> &T, vector<double> &dF_db,
        vector<vector<double>> &dS_db) {
  vector<double> PSI(NX / 4+1);
  int M_1 = 1, M_2 = 1;
  double dx = 4 * b[0] / NX;
  primal(NX, b, SX, SY, T, dS_db);
  double mid = T[NX / 8];
  for (int i = 0; i < 8; i++) {

    // RUNGE KUTTA
    double x, k1, k2, k3, k4;
    PSI[NX / 4 ] = 0;
    for (int r = NX / 4; r >= 1; r--) {
      x = r * dx;
      k1 = -dx * psi_deriv(b, M_1, M_2, SY[4 * r], x, T[2 * r], mid, PSI[r]);
      k2 = -dx * psi_deriv(b, M_1, M_2, SY[4 * r - 2], x - dx / 2, T[2 * r - 1],
                           mid, PSI[r] + k1 / 2);
      k3 = -dx * psi_deriv(b, M_1, M_2, SY[4 * r - 2], x - dx / 2, T[2 * r - 1],
                           mid, PSI[r] + k2 / 2);
      k4 = -dx * psi_deriv(b, M_1, M_2, SY[4 * (r - 1)], x - dx, T[2 * (r - 1)],
                           mid, PSI[r] + k3);
      PSI[r - 1] = PSI[r] + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
    }

    // S.D.'s
    double temp = 0;
    for (int l = 0; l < NX / 4; l++) {
      temp +=
          -(PSI[l] * (1 + b[1] * M_1) * pow(T[2 * l], 2) * dS_db[4 * l][i]) -
          (PSI[l + 1] * (1 + b[1] * M_1) * pow(T[2 * (l + 1)], 2) *
           dS_db[4 * (l + 1)][i]);
    }
    temp = dx * temp / 2;
    dF_db[i] = temp;
    temp = 0;
    if (i == 1) {
      for (int l = 0; l < NX / 4; l++) {
        temp += -(PSI[l] * M_1 * pow(T[2 * l], 2) * SY[4 * l]) -
                (PSI[l + 1] * M_1 * pow(T[2 * (l + 1)], 2) * SY[4 * (l + 1)]);
        temp = dx * temp / 2;
      }
    } else {
      temp = 0;
    }
    dF_db[i] += temp;
    temp = 0;
    if (i == 2) {
      for (int l = 0; l < NX / 4; l++) {
        temp += 4 * (PSI[l] * M_2 * T[2 * l] * SX[4 * l]) +
                4 * (PSI[l + 1] * M_2 * T[2 * (l + 1)] * SX[4 * (l + 1)]);
        temp = dx * temp / 2;
      }
    } else {
      temp = 0;
    }
    dF_db[i] += temp;
    temp = 0;
    if (i == 0) {
      temp = pow(T.back() - mid, 2) -
             PSI[0] * (-1 / pow(b[0], 2) + 50 / pow(b[0], 3));
    } else {
      temp = 0;
    }
    dF_db[i] += temp;
    cout << dF_db[i] << "\t";
  }
  cout << endl;
}

int main() {
  int NX = 400;
  vector<double> SX(NX+1, 0), SY(NX+1, 0);
  vector<double> T(NX / 2+1);
  vector<double> b(8), dF_db(8);
  vector<vector<double>> dS_db(NX+1, vector<double>(8, 0));
  datain(b);
  double hta = 0.08;
  cout << "ok" << endl;
  CA(NX, b, SX, SY, T, dF_db, dS_db);
  for (int i = 0; i < 17; i++) {
    for (int k = 0; k < b.size(); k++) {
      b[k] -= hta * dF_db[k];
    }
    CA(NX, b, SX, SY, T, dF_db, dS_db);
    cout << "EPANALHPSH" << i << endl;
  }
  for (int k = 0; k < 8; k++) {
    cout << b[k] << endl;
  }
  cout << primal(NX, b, SX, SY, T, dS_db);
}
